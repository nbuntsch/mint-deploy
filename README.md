# Usefull commands

## Install composer dependencies

```bash
cd path/to/site
composer install
```

## Launch Drupal with Docker

```bash
cd path/to/site
make up
```

## Install node dependencies

```bash
cd path/to/site/frontend
npm install
```

## Build frontend files

```bash
cd path/to/site/frontend
npm run build
```

## Watch frontend files

```bash
cd path/to/site/frontend
npm run watch
```

# Pipelines access

## Bitbucket Pipeline > Bitbucket Repository

1. In a Bitbucket repository, go to _**Repository** settings_ > _Pipelines_ > _SSH Keys_

2. Click on _Generate Keys_

3. Click on _Copy public key_

4. In Bitbucket _**Personal** settings_, go to _Security_ > _SSH Keys_

5. Click on _Add key_

6. Paste the public key

7. Click on _Add key_

## Bitbucket Pipeline > Server

### Add server to pipeline known hosts

1. In a Bitbucket repository, go to _**Repository** settings_ > _Pipelines_ > _SSH Keys_

2. Under _Known hosts_, type the website url

3. Click on _Fetch_

4. Click on _Add host_

### Add pipeline public key to server

1. Connect to the server with SSH

2. If it doesn't exist, create a _.ssh_ directory, in home directory

```bash
cd ~
mkdir .ssh
chmod -R 700 .ssh
```

3. If it doesn't exist, create an _authorized_keys_ file, in _.ssh_ directory

```bash
cd .ssh
vim authorized_keys
```

4. In a Bitbucket repository, go to _**Repository** settings_ > _Pipelines_ > _SSH Keys_

5. Click on _Copy public key_

6. Paste the public key in the _authorized_keys_ file

7. Set the access right of _authorized_keys_ to 600

```bash
chmod authorized_keys 600
```
